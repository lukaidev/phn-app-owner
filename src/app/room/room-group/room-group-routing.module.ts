import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoomGroupPage } from './room-group.page';

const routes: Routes = [
  {
    path: '',
    component: RoomGroupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoomGroupPageRoutingModule {}
