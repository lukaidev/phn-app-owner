import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoomGroupPageRoutingModule } from './room-group-routing.module';

import { RoomGroupPage } from './room-group.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoomGroupPageRoutingModule
  ],
  declarations: [RoomGroupPage]
})
export class RoomGroupPageModule {}
