import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoomAvailablePage } from './room-available.page';

const routes: Routes = [
  {
    path: '',
    component: RoomAvailablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoomAvailablePageRoutingModule {}
