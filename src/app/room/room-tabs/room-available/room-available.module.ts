import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoomAvailablePageRoutingModule } from './room-available-routing.module';

import { RoomAvailablePage } from './room-available.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoomAvailablePageRoutingModule
  ],
  declarations: [RoomAvailablePage]
})
export class RoomAvailablePageModule {}
