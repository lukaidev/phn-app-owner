import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoomOccupiedPage } from './room-occupied.page';

const routes: Routes = [
  {
    path: '',
    component: RoomOccupiedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoomOccupiedPageRoutingModule {}
