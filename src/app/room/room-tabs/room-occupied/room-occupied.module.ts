import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoomOccupiedPageRoutingModule } from './room-occupied-routing.module';

import { RoomOccupiedPage } from './room-occupied.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoomOccupiedPageRoutingModule
  ],
  declarations: [RoomOccupiedPage]
})
export class RoomOccupiedPageModule {}
