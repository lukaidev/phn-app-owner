import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RoomTabsPage } from './room-tabs.page';

const routes: Routes = [
  {
    path: '',
    component: RoomTabsPage,
    children: [
      {
        path: 'available',
        loadChildren: () => import('../room-tabs/room-available/room-available.module').then(m => m.RoomAvailablePageModule)
      },
      {
        path: 'occupied',
        loadChildren: () => import('../room-tabs/room-occupied/room-occupied.module').then(m => m.RoomOccupiedPageModule)
      },
      {
        path: '',
        redirectTo: 'available',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'available',
    pathMatch: 'full'
  },
  {
    path: 'room-occupied',
    loadChildren: () => import('./room-occupied/room-occupied.module').then( m => m.RoomOccupiedPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoomTabsPageRoutingModule {}
