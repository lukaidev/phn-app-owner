import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        const token = environment.token;
        const url = environment.url + req.url;
        const headers: HttpHeaders=  new HttpHeaders({
            Authorization: `Bearer ${token}`
        });

        const request = req.clone({
            headers,
            url
        });
        return next.handle(request);
    }

}