import { Component, Input, OnInit } from '@angular/core';
import { Owner } from 'src/app/owner/owner';

@Component({
  selector: 'app-owner-card',
  templateUrl: './owner-card.component.html',
  styleUrls: ['./owner-card.component.scss'],
})
export class OwnerCardComponent implements OnInit {

  @Input()
  owner: Owner;

  constructor() { }

  ngOnInit() {}

}
