import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Owner } from '../owner/owner';
import { OwnerService } from '../owner/owner.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  owner$ : Observable<Owner>;

  constructor(private ownerService: OwnerService) { }

  ngOnInit() {
    this.ownerService.loadOwner("xxxxx@gmail.com");
    this.owner$ = this.ownerService.owner$;
  }

}
