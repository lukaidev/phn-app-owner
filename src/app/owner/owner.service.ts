import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Owner } from './owner';
import { HttpClient } from '@angular/common/http';
import {catchError, map, shareReplay, tap} from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  private subject = new BehaviorSubject<Owner>(null);
  owner$ : Observable<Owner> = this.subject.asObservable();

  constructor(
    private http: HttpClient, 
    private router: Router) { }

  loadOwner(userEmail: string) {
    this.http.get<Owner>(`/owner/user/${userEmail}`)
    .pipe(
      catchError(err => {
        const message = "Could not load owner";
        console.log(message, err);
        return throwError(err);
      }),
      tap(owner => this.subject.next(owner)),
      shareReplay()
    ).subscribe();
  }

  getByUserRef(userRef: string) : Observable<Owner> {
    return this.http.get<Owner>(`/owner/user-ref/${userRef}`)
    .pipe(
      catchError(err => {
        const message = "Could not load owner";
        console.log(message, err);
        return throwError(err);
      }),
      tap(owner => this.subject.next(owner)),
      shareReplay()
    );
  }

  updateOwner(owner: Owner) {
    this.http.put<Owner>(`/owner`, owner)
    .pipe(
      catchError(err => {
        const message = "Unable to save owner";
        console.log(message, err);
        return throwError(err);
      }),
      tap(owner => this.subject.next(owner)),
    ).subscribe(() => this.router.navigate(['main', 'account']));
  }
  

}
