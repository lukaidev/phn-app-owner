
import { OwnerService } from './owner.service';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Owner } from './owner';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-owner',
  templateUrl: './owner.page.html',
  styleUrls: ['./owner.page.scss'],
})
export class OwnerPage implements OnInit {

  owner: Owner;
  ownerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private ownerService: OwnerService
  ) {

    this.ownerForm = this.formBuilder.group({
      id: ['', Validators.required],
      userRef: ['', Validators.required],
      email: ['', Validators.required],
      name: ['', Validators.required],
      mobileNo: ['', Validators.required]
    });

  }

  async loadRouteData() {
    const owner = await this.router.getCurrentNavigation().extras.state as Owner;
    if (owner) {
      this.populateForm(owner);
    } else {
      const userRef = this.route.snapshot.queryParamMap.get("u");
      this.ownerService.getByUserRef(userRef).subscribe(o => {
        this.populateForm(o);
      })
    }
  }

  populateForm(owner) {
    this.owner = owner;
    const form = this.ownerForm;
    form.controls['id'].setValue(owner.id);
    form.controls['userRef'].setValue(owner.userRef);
    form.controls['email'].setValue(owner.email);
    form.controls['name'].setValue(owner.name);
    form.controls['mobileNo'].setValue(owner.mobileNo);
  }

  ngOnInit() {
    this.loadRouteData();
  }

  submit() {
    const [owner, form] = [this.owner, this.ownerForm]
    const payload: Owner = {
      id: owner.id,
      userRef: owner.userRef,
      name: form.get('name').value,
      email: form.get('email').value,
      mobileNo: form.get('mobileNo').value,
      groups: owner.groups
    }
    this.ownerService.updateOwner(payload);
  }

}
