export interface Owner {

    id: string;
    userRef: string;
    name: string;
    email: string;
    mobileNo: string;
    groups: string[];

}