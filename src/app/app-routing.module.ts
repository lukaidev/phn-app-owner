import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'owner',
    loadChildren: () => import('./owner/owner.module').then( m => m.OwnerPageModule)
  },
  {
    path: 'main',
    loadChildren: () => import('./main-tabs/main-tabs.module').then( m => m.MainTabsPageModule)
  },
  {
    path: 'room',
    children: [
      {
        path: 'tabs',
        loadChildren: () => import('./room/room-tabs/room-tabs.module').then(m => m.RoomTabsPageModule)
      },
      {
        path: 'group',
        loadChildren: () => import('./room/room-group/room-group.module').then(m => m.RoomGroupPageModule)
      },
      {
        path: '',
        loadChildren: () => import('./room/room-tabs/room-tabs.module').then(m => m.RoomTabsPageModule)
      },
    ]
    
  },
  {
    path: '',
    loadChildren: () => import('./main-tabs/main-tabs.module').then( m => m.MainTabsPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then( m => m.AccountPageModule)
  },
  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
