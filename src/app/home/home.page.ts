import { Observable } from 'rxjs';
import { OwnerService } from './../owner/owner.service';
import { Component, OnInit } from '@angular/core';
import { Owner } from '../owner/owner';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
